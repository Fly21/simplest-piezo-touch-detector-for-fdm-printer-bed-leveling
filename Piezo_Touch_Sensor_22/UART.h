#ifndef UART_H
#define	UART_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h>

void UARTconfigure();
void SendByte(uint8_t Byte);

#ifdef	__cplusplus
}
#endif

#endif	/* UART_H */
